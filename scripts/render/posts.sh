source ./utils.sh
TEMP_PATH=/tmp/0u0.post.html
BASE_HTML=$1
POSTS_DIR=$2
OUTPUT_DIR=$3
mkdir -p $OUTPUT_DIR/blog
for p in $POSTS_DIR/*;
do
    bold_title $p > $TEMP_PATH
    POST_RENDERED="$(contentize $TEMP_PATH)"
    HTML=$(perl -pe "s|__BODY__|$POST_RENDERED|;s|__TITLE__|$(post_title $p)|" "$BASE_HTML")
    echo $HTML > $OUTPUT_DIR/blog/$(basename $p)
    rm $TEMP_PATH
done

