source ./utils.sh
TEMP_PATH=/tmp/0u0.index.html
BASE_HTML="$1"
TITLE="$2"
INDEX_HTML="$3"
INDEX_HTML_RENDERED="$(contentize_all $INDEX_HTML)"
perl -pe "s|__BODY__|$INDEX_HTML_RENDERED|;s|__TITLE__|$TITLE|" "$BASE_HTML"
