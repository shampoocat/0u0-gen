rm -rf output/
mkdir -p output
d="$(pwd)"
BASE_HTML="$d/templates/base.html"
cd scripts/render/ || exit
bash index.sh "$BASE_HTML" "Shampoocat's Website" "$d/templates/index.html" > $d/output/index.html
mkdir -p $d/output/blog
bash blog.sh "$BASE_HTML" "Shampoocat's Blog" "$d/templates/blog/index.html" "$d/posts" > $d/output/blog/index.html
bash posts.sh "$BASE_HTML" "$d/posts" "$d/output"
cd "$d"
cp -r static/* output/
